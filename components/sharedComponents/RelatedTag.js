import React , {Component} from 'react'
import {View, Text,Dimensions} from 'react-native';



const DEVICE_WIDTH = Dimensions.get('window').width


class RelatedTag extends Component{
    render(){
       const navigate = this.props.navigate;
        const topics = this.props.topics;
        return(
            <View style={{
                    borderTopColor: 'gray',
                    borderTopWidth: 5,
                    flex:1, 
                    flexDirection:'column'}}>
                <Text style={{fontSize:32,margin:10}}>Related Topics</Text>
                    {topics.map((tag,index) => {
                        return(
                            <View key={index} style={{width:DEVICE_WIDTH-50,  borderTopColor: 'black',borderTopWidth: 0.4,}}>
                                <Text style={{margin:10, textAlign:'left', fontSize:18, color:'red'}} 
                                onPress={()=>navigate('TagPosts',{tag:tag})}>{tag}</Text>
                            </View>
                        )
                    })}
            </View>
        )
    }
}
export default RelatedTag;