import React, { Component } from 'react'
import { WebView, View, Dimensions, Image, ScrollView, ImageBackground,StyleSheet,FlatList,Text } from 'react-native'
import { Loading } from './Loading';


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
class FullScreenImage extends Component {
    state = {
        index : 0
    }
    static navigationOptions = ({navigation})=>{
        const imageLength = navigation.getParam('image',null).length
        return{
            headerTitle:(
                <Text>{imageLength} {imageLength > 1 ? 'Images' : 'Image'}</Text>
            )
        }
    }
    render() {
        const image = this.props.navigation.getParam('image', null);
        const renderImage = ({item, index}) => {
            const image = {...item.image}
            return (
                <ScrollView 
                     maximumZoomScale={3} style={{backgroundColor:'black'}}>
                    <Image
                    onLoadStart = {() => <Loading/>}
                    source={{uri:image.src}} style={styles.image} />
                </ScrollView>
            )
        }
        return(
            <FlatList 
                data={image}
                keyExtractor={(item, index) => {
                    return(item.id.toString + index)
                }}
                renderItem = {renderImage}
                pagingEnabled={true}
                horizontal={true}
            />
        )
    }
}
const styles = StyleSheet.create({
    image:{
        flex: 1,
        position: 'relative',
        height: DEVICE_HEIGHT,
        width: DEVICE_WIDTH,
        justifyContent: 'center',
        alignSelf: 'stretch',
        alignItems: 'center',
        resizeMode: 'contain',
    }
})

export default FullScreenImage;