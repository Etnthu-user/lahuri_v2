import React,{Component} from 'react'
import {FlatList,View,Text,Dimensions,ScrollView, WebView,Image,StyleSheet,TouchableOpacity, Share} from 'react-native'
import {Icon} from 'react-native-elements'
import HTMLView from 'react-native-htmlview'
import { DateAndViews } from './DateAndViews';
import RelatedTag from './RelatedTag';
import { Footer } from './Footer';
import Sponsers from './Sponsers';



const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height



class DetailNews extends Component{
    static navigationOptions = ({ navigation }) => {
		return {
			title: '',
			headerRight: (
				<Icon
					name="share-square-o"
					type="font-awesome"
					color="white"
					size={30}
					iconStyle={{ paddingRight: 20,color: '#4682b4' }}
					onPress={navigation.getParam('shareNews')}
				/>
			)
		};
    };
    componentDidMount() {
        this.props.navigation.setParams({ shareNews: this.shareNews });
        
    }
	shareNews = () => {
		Share.share({
			title: 'Lahuri News',

			url: `http://lahuritv.net/${this.props.navigation.getParam('locale', null)}${this.props.navigation.getParam(
				'postUrl',
				null
			)}`
		});
	};
    render(){
        const {navigate} = this.props.navigation;
        const post = this.props.navigation.getParam('post',null);
        const RenderPostWithContent = ({item}) => {
            const postWithConent = {...item, ...item.thumbnailImages[0], ...item.postContents[0], ...item.isVideo}
            if(postWithConent.isVideo){
                return(
                    <View style={{flex:1, flexDirection:'column', }}>
                        <ScrollView
                            style={{flex:2,width:DEVICE_WIDTH, height:DEVICE_HEIGHT/2}}
                        >
                        <WebView style={{flex:2,aspectRatio:1.5}} source={{uri:postWithConent.videoEmbedUrl}}></WebView>
                        <Text style={{fontSize:22, textAlign:'center',fontWeight:'900',margin:5}} >{postWithConent.heading}</Text>
                        <View style={{marginLeft:20}}>
                             <DateAndViews 
                                navigate ={this.props.navigation.navigate}
                                tag={postWithConent.postTags.map(tag => {return tag.tagName})[0]} 
                                date = {postWithConent.publishedDate} />
                            </View> 
                            < Sponsers/>
                        <HTMLView
                            stylesheet={styles}
                            value={postWithConent.content}
                            ></HTMLView>
                        <RelatedTag navigate ={this.props.navigation.navigate}  topics = {postWithConent.postTags.map(topic => {return topic.tagName})} />
                        <Footer/>
                        </ScrollView>
                    </View>
                )
            }
            else{
                return(
                    <View style={{flex:1, flexDirection:'column', }}>
                        <ScrollView  style={{width:DEVICE_WIDTH, height:DEVICE_HEIGHT}}>
                            <TouchableOpacity onPress={() =>navigate('FullScreenImage',{image:item.thumbnailImages})}>
                                <Image
                                    style={{flex:2,width:DEVICE_WIDTH, height:DEVICE_HEIGHT/3,resizeMode:'contain'}}
                                    source={{uri: postWithConent.image.src}}>
                                </Image>
                            </TouchableOpacity>
                            <Text style={{fontSize:22, textAlign:'center',fontWeight:'900',margin:5}}>{postWithConent.heading}</Text>
                            <View style={{marginLeft:20}}>
                             <DateAndViews 
                                navigate ={this.props.navigation.navigate}
                                tag={postWithConent.postTags.map(tag => {return tag.tagName})[0]} 
                                date = {postWithConent.publishedDate} />
                            </View> 
                            < Sponsers/>
                            <HTMLView
                                addLineBreaks={false}
                                value={postWithConent.content}
                                stylesheet={styles}
                                >
                            </HTMLView>
                            <RelatedTag 
                                navigate ={this.props.navigation.navigate} 
                                topics = {postWithConent.postTags.map(topic => {return topic.tagName})} />
                            <Footer/>
                            
                        </ScrollView>
                    </View>
                )
            }
        }
        return(
            <React.Fragment>
           <FlatList
                ref={(ref) => { this.flatListRef = ref; }}
                data={post}
                renderItem={RenderPostWithContent}
                keyExtractor ={(item,index) => {return item.id.toString()+index}}
                horizontal={true}
                alwaysBounceHorizontal={true}
                pagingEnabled={true}
           ></FlatList>
           
           </React.Fragment>
        )
    } 
}
const styles = StyleSheet.create({
    p:{
        fontSize:18,
        padding: -100,
        margin:5
    }
})
export default DetailNews;