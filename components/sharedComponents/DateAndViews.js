import React from 'react'
import { Text, View, } from 'react-native'
import { Icon } from 'react-native-elements'
import moment from 'moment/min/moment-with-locales';
moment.locale('ne')
export const DateAndViews = (props) => {
    const date = props.date
    const navigate = props.navigate;
    if (date !==null && props.views !== null) {
        const formattedDate = moment(date).fromNow();
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end'
            }}>
                <Text style={{ flex: 2, textAlign: 'left', fontSize: 15,color:'#ff0000' }}>
                {formattedDate}{props.tag ?<Text onPress={() =>navigate('TagPosts',{tag:props.tag})}> | {props.tag}</Text> : null}</Text>
                {props.views ? <Icon name="eye" type="entypo" size={15} color='#ff0000' /> : null }
                <Text style={{ fontSize: 15,color:'#ff0000' }}> {props.views}</Text>
            </View>
        )
    }
}