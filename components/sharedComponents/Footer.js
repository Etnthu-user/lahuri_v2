import React from 'react'
import {View, Text} from 'react-native'
export const Footer = () => {
    return (
        <View style={{marginTop:30, marginBottom:10}}>
            <Text style={{textAlign: 'center', fontSize:18}}>Copyright{'\u00A9'} 2019 Lahuri TV</Text>
        </View>
    )
}