import React , {Component} from 'react'
import {connect} from 'react-redux'
import {View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions, Image, Linking, Alert} from 'react-native'
import { fetchSponsers } from '../../redux/ActionCreator';
import * as Animatable from 'react-native-animatable';
import {Card, Button, Icon} from 'react-native-elements';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const DEVICE_WIDTH = Dimensions.get('window').width;

const mapStateToProps = state => ({
    sponsers : state.sponsers
})

const mapDispatchToProps = dispatch => ({
    fetchSponsers : () => dispatch(fetchSponsers())
})


class Sponsers extends Component {
    constructor(props){
        super(props);
        this.state ={
            showModal : true
        }
    }

    componentDidMount(){
        this.props.fetchSponsers();
    }
    static navigationOptions = ({navigation}) =>({
        title : "Advertisement"
    })
    toggleModal(){
        this.setState({showModal : !this.state.showModal})
    }
    render(){
        const newList = new Array();
        if(this.props.sponsers && this.props.sponsers.advert.list){
            const sponserLength = this.props.sponsers.advert.list.length;
            let newIndex = Math.ceil(Math.random() * sponserLength)
            if(newList.length < 1) {
                newList.push(this.props.sponsers.advert.list[newIndex]);    
                }
            }
        const sponser = {...newList[0]};
        if (sponser.hasOwnProperty('title')){
            return(
                <Animatable.View animation='zoomIn'duration={500} style={{
                flex:1, width:DEVICE_WIDTH-30, 
                backgroundColor:'#9b9b9b',
                margin:15,
                height:DEVICE_HEIGHT/5,flexDirection:'column',
                borderTopWidth:1,
                borderTopColor:'black',
                borderBottomColor:'black',
                borderBottomWidth:1,
                justifyContent:'center'}}>
                <TouchableOpacity style={{flex:2,}} onPress={() => 
                                    Alert.alert(
                                    'Visit a website',
                                    "You will be redirected to the advertiser's website",
                                    [
                                        {
                                            text: 'Cancel',
                                            onPress: () => this.toggleModal(),
                                            style: 'cancel'
                                        },
                                        {
                                            text: 'Ok',
                                            onPress: () => {Linking.openURL(sponser.adUrl), this.toggleModal()}
                                        }
                                    ],
                                    {cancelable:false}
                                    )
                            }>
                    <Text style={{fontSize:10, textAlign:'right'}}>Advertisement</Text>
                    <Text style={styles.text}>{sponser.title}</Text>
                    <Image style={{flex:3, width:DEVICE_WIDTH-30, height:DEVICE_HEIGHT/2,resizeMode:'contain'}} source={{uri:sponser.thumbnailImageUrl}}></Image>
                    <Text style={styles.text}>{sponser.tagLine}</Text>
                </TouchableOpacity>
                </Animatable.View>
            )
        }
        else{
            return null
        }

    }
}
const styles = StyleSheet.create({
    text:{
        textAlign:'center', 
        fontSize:20, 
        
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Sponsers);