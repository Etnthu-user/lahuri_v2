import React, {Component} from 'react'
import {ActivityIndicator,View,Text} from 'react-native'
import { color } from '../../shared/extra';


export const Loading = () =>{
    return(
        <View style={{flex:1,justifyContent:'center'}}>
                <ActivityIndicator size='large' color={color} />
        </View>
    )
}