import React  from 'react';
import {Image, View, Text} from 'react-native';
import {createMaterialTopTabNavigator,
        createStackNavigator,
        createSwitchNavigator,
        createAppContainer,
        } from 'react-navigation'
import News from './News';
import { Icon } from 'react-native-elements';
import Popular from './popular/Popular'
import Shows from './shows/Shows';
import Contact from './Contact';
import Search from './Search';
import Seasons from './shows/Seasons'
import RenderEpisode from './shows/RenderEpisode';
import DetailNews from './sharedComponents/DetailNews';
import FullScreenImage from './sharedComponents/FullScreenImage';
import TagPosts from './Tagposts';
import Sponsers from './sharedComponents/Sponsers';
const newsNavigator = createSwitchNavigator(
    {
        News : {
            screen :News,

        },

    },

)

const popularPostsNavigator = createSwitchNavigator(
    {
        PopularPosts: {
            screen: Popular,
        },
        
    },
)

const showsNavgator = createSwitchNavigator(
    {
        Shows :{
            screen:Shows,
        }
    },
    {
        initialRouteName:'Shows'
    }
    
)
const contactNavigator = createSwitchNavigator(
    {
        Contact:{
            screen:Contact
        }
    }
)
const serchNavigator = createSwitchNavigator(
    {
        Search:{
            screen:Search
        }
    },
    {   
        navigationOptions: ({navigation}) => ({
            headerRight:(<Icon name="cross" type="entypo" 
            size={30} onPress={() => navigation.navigate('News')}></Icon>),
            headerTitle:'Search'
        })
    }
);

const MainNavigator = createMaterialTopTabNavigator(
    {
        News:{
            screen:newsNavigator,
        },
        Popular:{
            screen:popularPostsNavigator
        },
        Shows:{
            screen:showsNavgator
        },
        Contact:{
            screen: contactNavigator
        },
    },
    { 
        tabBarOptions:{
           
            upperCaseLabel:false,
            scrollEnabled:true,
            allowFontScaling:true,
            
            labelStyle:{
                fontSize:18,
                
            },
            
            style:{
                backgroundColor:'#0864af', 
            },
            indicatorStyle:{
                backgroundColor:'#db3d3d',
                height:5
            }

        },
        initialRouteName:'News',
        navigationOptions:({navigation})=>({
            headerRight: ( 
                <Icon name="search" size={30} color="white" onPress={() => navigation.navigate('Search')} />),
            headerTitle:(
                <Image style={{display:'flex', alignSelf:'center',height:60,width:60, resizeMode:'contain'}} 
                source={require('../assets/ltv-logo.png')}></Image>
                ),
                headerStyle:{
                backgroundColor:'#4682b4'
            },
        })   
    }
)
const navigator = createStackNavigator(
    {
        Home:{
            screen:MainNavigator
        },
        Search:{
            screen:serchNavigator
        },
        Seasons:{
            screen:Seasons
        },
        RenderEpisode:{
            screen: RenderEpisode
        },
        DetailNews:{
            screen:DetailNews
        },
        FullScreenImage: {
            screen: FullScreenImage
        },
        Sponsers: {
            screen: Sponsers
        },
        TagPosts:{
            screen: TagPosts
        },
    },
)

export default createAppContainer(navigator);