import React, {Component} from 'react';
import {Text,FlatList } from 'react-native';
import {connect} from 'react-redux'
import { fetchShows, showsFailed } from '../../redux/ActionCreator';
import {Tile} from 'react-native-elements'
import { Loading } from '../sharedComponents/Loading';

const mapStateToProps = state =>({
    shows:state.shows
})
const mapDispatchToProps = dispatch => ({
    fetchShows: ()=> dispatch(fetchShows())
})
class Shows extends Component{
    componentDidMount(){
        this.props.fetchShows()
    }
    render(){
        const {navigate} = this.props.navigation
        const renderShows =({item}) => {
            const show  = {...item,...item.thumbnailImages[0]}
            return(
                <Tile title={show.name} imageSrc={{uri:show.image.src}}titleStyle={{textAlign:'center'}}
                onPress={()=>navigate('Seasons',{showName:show.name,showId:item.id,image:show.image.src,details:show.details})}
                ><Text style={{margin:5,textAlign:'center',fontSize:15}}>{show.details}</Text></Tile>
            )
        }
        if(this.props.shows.isLoading){
            return(
                <Loading/>
            )
        }
        else if(this.props.shows.shows && this.props.shows.shows.length){
            return(
                <FlatList 
                    data={this.props.shows.shows}
                    renderItem = {renderShows}
                    keyExtractor={(item) => { return item.id.toString()}}
                    alwaysBounceHorizontal
                    
                />
            )

        }
        else{
            return(
                <Text style={{fontSize:25,textAlign:'center',marginTop:300}}>Something is Wrong here.
                If that's the case,then it's being fixed.{'\n'}
                Thank you for understanding.</Text>
            )
        }   

        }
}
export default connect(mapStateToProps,mapDispatchToProps)(Shows);