import React, {Component} from 'react'
import {Text,View,FlatList,Dimensions,TouchableOpacity, ScrollView  } from 'react-native'
import {connect} from 'react-redux'
import {Button,Card,Tile} from 'react-native-elements'
import { fetchSeasons, fetchEpisodes } from '../../redux/ActionCreator';
import Sponsers from '../sharedComponents/Sponsers';
import {Footer} from '../sharedComponents/Footer'


const mapStateToProps = state =>({
    seasons :state.seasons,
    episodes:state.episodes
})
const mapDispatchToProps = dispatch =>({
    fetchSeasons: (showId) => dispatch(fetchSeasons(showId)),
    fetchEpisodes:(seasonId,Page,append)=> dispatch(fetchEpisodes(seasonId,Page,append))
})


class Seasons extends Component {

    constructor(props){
        super(props);
        this.state={
            selectedSeason: null
        }
    }
    
    static navigationOptions = ({navigation}) =>{
        return{
            title : navigation.getParam('showName','')
        }
    }
    componentDidUpdate(prevProps, prevState) {
		if (prevProps.seasons.seasons !== this.props.seasons.seasons) {
			const selectedSeason =
				this.props.seasons.seasons && this.props.seasons.seasons[0] && this.props.seasons.seasons[0];
			this.setState({ selectedSeason });
			this.getEpisodes(selectedSeason.id);
		}
	}
    componentDidMount(){
        this.props.fetchSeasons(this.props.navigation.getParam('showId',null))
    }
    changeSeason = (season) =>{
        this.getEpisodes(season.id)
        this.setState({ selectedSeason: season })
    }
    getEpisodes = (seasond) => {
		this.props.fetchEpisodes(seasond, '');
	};
	changeSeason = (season) => {
		this.getEpisodes(season.id);
		this.setState({ selectedSeason: season });
	};
	handleRefresh = () => {
		this.setState(
			{
				refreshing: this.props.episodes.refreshing
			},
			() => this.props.fetchEpisodes(this.props.seasons.seasons[0].id, '')
		);
	};
	loadMore = () => {
		if (!this.props.episodes.isLastpage && this.props.episodes.nextPage !== undefined && this.props.episodes.nextPage !== null) {
			this.props.fetchEpisodes(this.props.seasons.seasons[0].id, this.props.episodes.nextPage, true);
		}
	};
    
    render (){
        const {navigate} = this.props.navigation
        const renderEpisodes = ({item,index}) =>{
            const episode = {...item, ...item.thumbnailImages[0]}
            const titleName = episode.title.split('Australia React');
            const title =titleName.join('').replace(/[.,\/#|!||?$%\^&\*;:{}=\-_`~()]/g," ")
            const filteredTitle = title.replace(/\s{2,}/g," ")
            return(
                <View style={{flex:1,flexDirection:'column',width:Dimensions.get('window').width/3}}>
                    <TouchableOpacity onPress={() => navigate('RenderEpisode',{item:item.videoUrl})}>                    
                        <Card  image={{uri:episode.image.src}}></Card>
                    </TouchableOpacity>

                </View>      
            )
            
        }
        if(this.props.seasons.seasons.length || this.props.episodes.refreshing){
            return(
                <ScrollView style={{backgroundColor:'black',height:Dimensions.get('window').height}}>
                    <View style={{flexDirection:'row',justifyContent:'center',marginTop:20}}>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                        <Text style={{fontSize:30,color:'white',textAlign:'center'}}>Seasons </Text>
                        {this.props.seasons.seasons.map(season =>{
                            return(
                                <Button 
                                 key={season.id} title={season.name}
                                 onPress={()=>this.changeSeason(season)}
                                 buttonStyle={{					
                                    borderRadius: 30,
                                    width: 40,
                                    margin: 2,
                                    borderColor: 'black',
                                    backgroundColor: this.state.selectedSeason === season ? '#4682b4' : 'gray'}} 
                                  >
                                  </Button>   
                            )
                        })}
                        </View>
                    </View>
                    <Sponsers/>
                    <Tile 
                        title={this.props.navigation.getParam('details',null)}
                        titleStyle={{color:'white',textAlign:'center',fontSize:15}}
                        containerStyle={{margin:5}}
                        imageSrc={{uri:this.props.navigation.getParam('image',null)}}>
                    </Tile>
                <Text style={{color:"white",fontSize:15,marginLeft:20}}>{this.state.selectedSeason ? this.state.selectedSeason.episodesCount + ' Episodes' : null}</Text>
                <FlatList
                    data={this.props.episodes.episodes}
                    renderItem={renderEpisodes}
                    keyExtractor={(item,index) => {return item.id.toString() + index}}
                    onRefresh={this.handleRefresh}
                    refreshing={this.props.episodes.refreshing}
                    onEndReachedThreshold={0}
                    onEndReached={this.loadMore}
                    horizontal
                    >
                </FlatList>
                <Footer/>
            </ScrollView>
            )
        }
       
        else {
             return <Text style={{ fontSize: 30, textAlign: 'center', paddingTop: 350 }}>No Seasons Found.</Text>;
            }
        
    
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Seasons)