import React,{Component} from 'react';
import {View,Text,Button,WebView} from 'react-native'

class RenderEpisode extends Component{
    render(){
        const episode = this.props.navigation.getParam('item',null)
        return(
       
            <WebView source={{uri:episode}}
                contentInset={{		 
                    bottom: 50,
                   
                    }}
            ></WebView>
       
       
        )
    }
}

export default RenderEpisode    