import React, {Component} from 'react'
import {Text,FlatList,TouchableOpacity,View,StyleSheet,Dimensions} from 'react-native'
import {Card, } from 'react-native-elements'
import {connect} from 'react-redux';
import { fetchTagPosts } from '../redux/ActionCreator';
import { Loading } from './sharedComponents/Loading';
import {DateAndViews} from './sharedComponents/DateAndViews'

const mapStateToProps = state => ({
    tagPosts :state.tagPosts
})
const mapDispactchToProps = dispatch => ({  
    fetchTagPosts : (statusId, Tags, Page, append) => dispatch(fetchTagPosts(statusId, Tags, Page, append))
})

class TagPosts extends Component {
    constructor(props){
        super(props);
        this.state={
            statusId: 2,
            tagName: this.props.navigation.getParam('tag',null)
        }
    }
    static navigationOptions = ({navigation}) => ({
        title : navigation.getParam('tag',null)
    })
    componentDidMount(){
        this.props.fetchTagPosts(this.state.statusId,this.state.tagName,1)
    }
    handleRefresh = () =>{
        this.setState(
            {
                refreshing: this.props.tagPosts.refreshing,
            },
            () => this.props.fetchTagPosts(this.state.statusId, this.state.tagName,1)
        )
    }
    loadMore = () => {
        if(!this.props.tagPosts.isLastPage){
            this.props.fetchTagPosts(this.state.statusId,this.state.tagName, this.props.tagPosts.nextPage,true)
        }
    }
    render(){
        const {navigate} = this.props.navigation
        const renderTagPosts = ({item, index}) => {
            if(item && item.thumbnailImages[0] && item.postContents[0]){
                const post = {...item, ...item.thumbnailImages[0], ...item.postContents[0], ...item.isVideo}
                return(
                    <TouchableOpacity onPress={() => navigate('DetailNews',
                    {
                        post:this.props.tagPosts.posts.slice(index, this.props.tagPosts.posts.length),
                        postUrl: post.postUrl,
                        locale: post.locale.localeName.toLowerCase(),
                    }) }>
                    <View style={styles.container}>
                    
                        <Card containerStyle={styles.cardContainer}
                         image={{uri:post.image.src}}
                         imageStyle={styles.imageContainer}
                         >  
                        <Text style={styles.textContainer}>{post.heading}</Text>
                        <View style={{flex:2,width:150,marginLeft:Dimensions.get('window').width/2.5}}>
                        <DateAndViews 
                            navigate ={this.props.navigation.navigate}
                            // tag={post.postTags.map(tag=>{return tag.tagName})[0]} 
                            date ={post.publishedDate} /> 
                        </View>           
                    </Card>     
                    
                    </View>
                 </TouchableOpacity>
                )
            }
            else{
                return(
                    <Text style={{textAlign:'center', marginTop:10, fontSize:30}}> Could not load properly here!!!</Text>
                )
            }
  
        }
        if(this.props.tagPosts && this.props.tagPosts.posts[0]){
            return (
                <FlatList
                    data={this.props.tagPosts.posts}
                    renderItem={renderTagPosts}
                    keyExtractor = {(item,index) => {return item.id.toString() + index}}
                    refreshing ={this.props.tagPosts.refreshing}
                    onRefresh={this.handleRefresh}
                    onEndReached={this.loadMore}
                    onEndReachedThreshold={5}
                ></FlatList>
            )
        }
        if(this.props.tagPosts.isLoading){
            return(
                <Loading/>
            )
        }
        else{
            return(
                <Text style={{textAlign:'center', marginTop:350, fontSize:30}}> OOps!! Something is wrong. </Text>
            )
        }
 
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        width:Dimensions.get('window').width
    },
    cardContainer:{
        flex:1,
        flexDirection:'row',
        width:Dimensions.get('window').width/2, 
        height:Dimensions.get('window').height/7,
        flexGrow:1,
        marginHorizontal: 5,
		marginVertical: 10,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: '#4682b4',
		shadowOpacity: 0.5
    },
    imageContainer:{
        width:Dimensions.get('window').width/2.5,
        height:(Dimensions.get('window').height/7)-1
    },
    textContainer:{
        flex:3,
        marginLeft:Dimensions.get('window').width/2.5,
        marginTop:-100,
        fontSize:15
    }
})

export default connect(mapStateToProps,mapDispactchToProps)(TagPosts);