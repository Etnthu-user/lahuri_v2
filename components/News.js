import React, {Component } from 'react';
import {Text,FlatList} from 'react-native'
import {Tile} from 'react-native-elements'
import {connect} from 'react-redux'
import { fetchPosts, increaseView } from '../redux/ActionCreator';
import { Loading } from './sharedComponents/Loading';
import { DateAndViews } from './sharedComponents/DateAndViews';
import {Permissions, Notifications} from 'expo'


const mapStateToProps = state => ({
    posts : state.posts
})
const mapDispactchToProps = dispatch =>({
    fetchPosts : (statusId,page,append) => dispatch(fetchPosts(statusId,page, append)),
    increaseView: (postId) => dispatch(increaseView(postId))
})
class News extends Component{
    constructor(props){
        super(props)
        this.state={
            statusId:2
        }
    }
    componentDidMount(){
        this.props.fetchPosts(this.state.statusId, 1)
    }
    handleRefresh = () => {
        this.setState({
            refreshing: this.props.posts.refreshing,
        },
        ()=>this.props.fetchPosts(this.state.statusId,1))
    }
    loadMore = () =>{
        if(!this.props.posts.isLastPage){
            this.props.posts.refreshing,
            this.props.fetchPosts(this.state.statusId, this.props.posts.nextPage, true)
        }
    }
    async componentWillReceiveProps(nextProps){
        if(nextProps.posts.posts !== this.props.posts.posts){
          await this.presentLocalNotification();
        }
    }
    handleNotification = () => {
        return this.props.navigation.navigate('DetailNews',{post:this.props.posts.posts.slice(0,1)});
    }
    async obtainNotificationPermission(){
        let permission = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        if(permission.status !== 'granted'){
            permission = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            if(permission.status !== 'granted'){
                Alert.alert('Permisson not granted to show the notifications.')
            }
        }
        Notifications.addListener(this.handleNotification)
        return permission;
    }

    async presentLocalNotification(){
        await this.obtainNotificationPermission();
        const firstPost = this.props.posts.posts.slice(0,1)
        const details = firstPost[0].postContents[0].heading
        const date = new Date().getTime()+ 10000;
        // console.log(date);
        // date.setTime(date.getTime()+ 311100000)
        Notifications.scheduleLocalNotificationAsync({
            title:'Lahuri News',
            body: '' +  details,
            ios: {
                sound: false
            },
            android:{
                sound:true,
                vibrate:true,
            }
        },
        {
            time: date,
            repeat:'day'
        })
    }
    render(){
        const {navigate} = this.props.navigation;
        const RenderPosts = ({item,index })=>{
            const news = {...item, ...item.thumbnailImages[0], ...item.postContents[0],...item.isVideo}
            return(
                <React.Fragment>
                <Tile
                    
                    icon={news.isVideo ? {name:'youtube-play', type:'font-awesome', size:80, color:'white'} : null}
                    title={news.heading}
                    titleStyle={{textAlign:'center'}}
                    imageSrc={{uri:news.image.src}}
                    onPress={()=> {
                        this.props.increaseView(item.id);
                        navigate('DetailNews',
                    {
                        post:this.props.posts.posts.slice(index, this.props.posts.posts.length),
                        postUrl: news.postUrl,
                        locale: news.locale.localeName.toLowerCase(),
                    })}}
                ></Tile>
                <DateAndViews  
                    navigate ={this.props.navigation.navigate}
                    tag={news.postTags.map(tag => {return tag.tagName})[0]} 
                    date={news.publishedDate} views={news.views}/>

                </React.Fragment>
            )
        }
        if(this.props.posts.posts && this.props.posts.posts.length){
            return(
            <React.Fragment>
            <FlatList
                data={this.props.posts.posts}
                renderItem={RenderPosts}
                keyExtractor={(item,index) => {return item.id.toString()+ index}}
                onRefresh={this.handleRefresh}
                refreshing={this.props.posts.refreshing}
                onEndReached={this.loadMore}
                onEndReachedThreshold={25}
            ></FlatList>
            </React.Fragment>
            )
        }
        else if(this.props.posts.isLoading){
            return(<Loading/>)
        }
        else{
        return(
            <Text style={{textAlign:'center',marginTop:250, fontSize:25}}>Oops!! Something's wrong.
                If you are seeing this messsage then it's being fixed. Thank you for you patience</Text>
        )
        }
    }
}
export default  connect(mapStateToProps,mapDispactchToProps)(News);
