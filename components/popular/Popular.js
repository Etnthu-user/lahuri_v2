import React, {Component} from 'react'
import {connect} from 'react-redux'
import {View,FlatList,Text, ScrollView,StyleSheet,Dimensions,TouchableOpacity, Alert, PushNotificationIOS} from 'react-native'
import {Tile,Card} from 'react-native-elements'
import { fetchPopular } from '../../redux/ActionCreator';
import { Loading } from '../sharedComponents/Loading';
import { DateAndViews } from '../sharedComponents/DateAndViews';


const mapStateToProps = state =>({
    popular : state.popular
})

const mapDispatchToProps = dispatch =>({
    fetchPopular : () => dispatch(fetchPopular())
})

class Popular extends Component{

    async componentDidMount(){
        await this.props.fetchPopular();
    }

    render(){
        const {navigate} = this.props.navigation
        const RenderPopular = ({item, index}) => {
            const post = {...item, ...item.thumbnailImages[0], ...item.postContents[0], ...item.isVideo}
            return(
                <TouchableOpacity onPress={() => {
                    navigate('DetailNews',
                {
                    post:this.props.popular.posts.slice(index+1, this.props.popular.posts.length),
                    postUrl: post.postUrl,
                    locale: post.locale.localeName.toLowerCase(),
                }) }}>
                <View style={styles.container}>
                
                    <Card containerStyle={styles.cardContainer}
                     image={{uri:post.image.src}}
                     imageStyle={styles.imageContainer}
                     >  
                    <Text style={styles.textContainer}>{post.heading}</Text>
                    <View style={{flex:2,width:150,marginLeft:Dimensions.get('window').width/2.5}}>
                    <DateAndViews 
                        navigate ={this.props.navigation.navigate}
                        tag={post.postTags.map(tag=>{return tag.tagName})[0]} 
                        date ={post.publishedDate} /> 
                    </View>           
                </Card>     
                
                </View>
            </TouchableOpacity>
            )
        }
        if(this.props.popular && this.props.popular.posts[0]){
            const firstPost = this.props.popular.posts.slice(0,1)
            const firstItem = {...firstPost,...firstPost[0].thumbnailImages[0], ...firstPost[0].postContents[0]}
            return(
                <ScrollView style={{backgroundColor:'#cfd3d3'}}>
                <View>
                    <Tile 
                        title={firstItem.heading}
                        titleStyle={{textAlign:'center'}}
                        imageSrc={{uri: firstItem.image.src}}
                        onPress={()=> navigate('DetailNews',{post:this.props.popular.posts.slice(0, this.props.popular.posts.length)})}
                    ></Tile>
                    <DateAndViews 
                        navigate ={this.props.navigation.navigate}
                        tag={firstPost[0].postTags.map(tag=>{return tag.tagName})[0]} 
                        date={firstPost[0].publishedDate} views = {firstPost[0].views} />
                    </View>
                <FlatList
                    data={this.props.popular.posts.slice(1,this.props.popular.posts.length)}
                    renderItem={RenderPopular}
                    keyExtractor={(item, index) => {return item.id.toString() + index}}
                ></FlatList>
                </ScrollView>
            )
        }
        else if(this.props.popular.isLoading){
            return(<Loading/>)
        }
        else{
            return(
                <View>
                <Text style={{fontSize:25, marginTop:250, textAlign:'center'}}>
                Oops!! There is nothing to show here at this time.</Text>
                </View>
            )
        }
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        width:Dimensions.get('window').width
    },
    cardContainer:{
        flex:2,
        flexDirection:'row',
        width:Dimensions.get('window').width/2, 
        height:150,
        flexGrow:1,
        marginHorizontal: 5,
		marginVertical: 10,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: '#4682b4',
		shadowOpacity: 0.5
    },
    imageContainer:{
        width:Dimensions.get('window').width/2.5,
        height:149
    },
    textContainer:{
        marginLeft:Dimensions.get('window').width/2.5,
        marginTop:-145,
        fontSize:18
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Popular);