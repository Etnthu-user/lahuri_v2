export const POST_LOADING = "POST_LOADING";
export const GET_POSTS  ="GET_POSTS";
export const POSTS_FAILED = "POSTS_FAILED";

export const POPULAR_POST_LOADING = "POPULAR_POST_LOADING";
export const GET_POPULAR_POST = "GET_POPULAR_POST";
export const POPULAR_POST_FAILED = "POPULAR_POST_FAILED";

export const GET_SHOWS = "GET_SHOWS";
export const SHOWS_LOADING = "SHOWS_LOADING";
export const SHOWS_FAILED = "SHOWS_FAILED";

export const GET_SEASONS = "GET_SEASONS";
export const SEASONS_LOADING = "SEASONS_LOADING";
export const SEASONS_FAILED = "SEASONS_FAILED"


export const GET_EPISODES = "GET_EPISODES"
export const EPISODES_LOADING = "EPISODES_LOADING"
export const EPISODES_FAILED = "EPISODES_FAILED"


export const GET_TAGPOSTS = "GET_TAGPOSTS"
export const TAGPOSTS_LOADING = "TAGPOSTS_LOADING"
export const TAGPOSTS_FAILED = "TAGPOSTS_FAILED"


export const GET_SPONSERS = "GET_SPONSERS"
export const SPONSERS_LOADING = "SPONSERS_LOADING"
export const SPONSERS_FAILED = "SPONSERS_FAILED"


export const GET_POPULAR_TAGS = "GET_POPULAR_TAGS"
export const POPULAR_TAGS_LOADING = "POPULAR_TAGS_LOADING"
export const POPULAR_TAGS_FAILED = "POPULAR_TAGS_FAILED"


export const GET_SEARCHRESULTS = 'SEARCH_RESULTS';
export const SEARCH_LOADING = 'SEARCH_LOADING';
export const SEARCH_FAILED = 'SEARCH_FAILED';
export const SEARCH_CLEAR_RESULTS = 'SEARCH_CLEAR_RESULTS';

export const INCREASE_VIEW = "INCREASE_VIEW";