import * as ActionTypes from './ActionTypes'
import {baseUrl} from '../shared/baseUrl'
//Get post 
// baseUrl/post?StatusId&variousParams
export const fetchPosts = (statusId,Page,append) => dispatch =>{
    dispatch(postsLoading());
    return fetch(baseUrl + `post?StatusId=${statusId}&Page=${Page}`)
        .then(response => {
            if(response.ok){
                return response;
            }
            else{
                var error = new Error("Error " + response.status + ' : ' + response.statusText);
                error.message = response;
                throw error
            }
        },error => {
            var err = new Error(error.message);
            throw err;
        })
        .then(response => response.json())
        .then(posts => dispatch(getPosts(posts,append)))
        .catch(err => dispatch(postsFailed(err.message)))
}

export const postsLoading = () => ({
    type : ActionTypes.POST_LOADING
})
export const getPosts = (posts,append) => ({
    type : ActionTypes.GET_POSTS,
    payload : {posts,append}
})
export const postsFailed = (err) => ({
    type : ActionTypes.POSTS_FAILED,
    payload: err
})

//Get Popular
// baseUrl/post/popular/20

export const fetchPopular = (count=20)=> dispatch =>{
    dispatch(popularPostLoading)
    return fetch(baseUrl+`post/popular/${count}`)
        .then(response =>{
            if(response.ok){
                return response;
            }
            else{
                var error = new Error('Error' + response.status + ' : ' + response.statusText)
                error.message = response;
                throw error;
            }   
        }, err => {
            var error = new Error(err.message)
            throw error
        })
        .then(response => response.json())
        .then(popular => dispatch(getPopularPosts(popular)))
        .catch(error => dispatch(popularFailed(error.message)))
}
export const popularPostLoading = () => ({
    type : ActionTypes.POPULAR_POST_LOADING
})
export const getPopularPosts = (posts) => ({
    type : ActionTypes.GET_POPULAR_POST,
    payload: posts
})
export const popularFailed = (err) => ({
    type : ActionTypes.POPULAR_POST_FAILED,
    payload : err
})

//Get poular tagPosts
export const fetchTagPosts = (statusId, Tags, Page, append) => dispatch => {
    dispatch(tagPostsLoading())
    return fetch(baseUrl + `post?StatusId=${statusId}&Tags=${Tags}&Page=${Page}`)
        .then(response => {
            if(response.ok){
                return response
            }
            else{
                var error = new Error('Error ' + response.status + ' + ' +response.statusText)
                error.message = response;
                throw error
            }
        }, err => {
            var errMess = new Error(err.message)
            throw errMess
        })
        .then(response =>response.json())
        .then(tagPosts => dispatch(getTagPosts(tagPosts,append)))
        .catch(err => dispatch(tagPostsFailed(err.message)))
}
export const tagPostsLoading = () => ({
    type :ActionTypes.TAGPOSTS_LOADING
})
export const getTagPosts = (tagPosts, append) => ({
    type : ActionTypes.GET_TAGPOSTS,
    payload: {tagPosts, append}
})
export const tagPostsFailed = (err) => ({
    type : ActionTypes.TAGPOSTS_FAILED,
    payload: err
})
//Get shows
//baseUrl/shows

export const fetchShows = () => dispatch =>{
    dispatch(showsLoading());
    return fetch(baseUrl + 'shows')
        .then(response => {
            if(response.ok){
                return response;
            }
            else{
                var error = new Error("Error " + response.status + ' : ' + response.statusText);
                error.message = response;
                throw error;
            }
        }, err => {
            var error = new Error(err.message);
            throw error;
        })
        .then(response => response.json())
        .then(shows => dispatch(getShows(shows)))
        .catch(err => dispatch(showsFailed(err.message)))
}

export const showsLoading = () => ({
    type : ActionTypes.SHOWS_LOADING
})

export const getShows = (shows) => ({
    type: ActionTypes.GET_SHOWS,
    payload: shows
})
export const showsFailed = (err) => ({
    type : ActionTypes.SHOWS_FAILED,
    payload:err
})

//Get seasons
//baseUrl/shows/{showId}/seassons
export const fetchSeasons = (showId) => dispatch => {
    dispatch(seasonsLoading())
    return fetch(baseUrl + `shows/${showId}/seasons`)
        .then(response =>{
            if(response.ok){
                return response
            }
            else{
                var error = new Error('Error ' + response.status + ' : ' + response.statusText);
                error.message = response;
                throw error;
            }
        },err => {
            var error = new Error(err.message)
            throw error;
        })
        .then(response => response.json())
        .then(seasons => dispatch(getSeasons(seasons)))
        .catch(err => dispatch(seasonsFailed(err.message)))

}

export const seasonsLoading = () => ({
    type:ActionTypes.SEASONS_LOADING
})

export const getSeasons = (seasons) => ({
    type: ActionTypes.GET_SEASONS,
    payload:seasons
})
export const seasonsFailed = (err) => ({
    type:ActionTypes.SEASONS_FAILED,
    payload:err
})

export const fetchEpisodes = (seasonId,Page,append) => dispatch => {
    dispatch(episodesLoading())
    return fetch(baseUrl + `seasons/${seasonId}/episodes?Page=${Page}`)
        .then(response => {
            if(response.ok){
                return response;
            }
            else{
                var error = new Error('Error ' + response.status + ' : ' + response.statusText);
                error.message = response;
                throw error;
            }
        },err => {
            var error = new Error(err.message)
            throw error;
        })
        .then(response => response.json())
        .then(episodes => dispatch(getEpisodes(episodes,append)))
        .catch(err => dispatch(episodesFailed(err.message)))
}

export const episodesLoading = () => ({
    type : ActionTypes.EPISODES_LOADING
})

export const getEpisodes = (episodes,append) => ({
    type : ActionTypes.GET_EPISODES,
    payload:{episodes,append}
})
export const episodesFailed = (err) => ({
    type: ActionTypes.EPISODES_FAILED,
    payload:err
})

//Get sponsers 

export const fetchSponsers  = () => dispatch =>{
    dispatch(sposnersLoading())
    return fetch(baseUrl + 'sponsers')
        .then(response => {
            if(response.ok){
                return response
            }
            else{
                var error = new Error('Error ' + response.status + ' : ' + response.statusText)
                error.message = response
                throw error
            }
        }, err => {
            var error = new Error(err.message)
            throw error
        })
        .then(response => response.json())
        .then(sponsers => dispatch(getSponsers(sponsers)))
        .catch(err => dispatch(sponsersFailed(err.message)))
}
export const sposnersLoading = () =>({
    type : ActionTypes.SPONSERS_LOADING
})
export const getSponsers = (sponsers) => ({
    type : ActionTypes.GET_SPONSERS,
    payload : sponsers
})
export const sponsersFailed = (err) => ({
    type : ActionTypes.SPONSERS_FAILED,
    payload: err
})

export const fetchPopularTags = () => dispatch => {
    dispatch(popularTagsLoading())
    return fetch(baseUrl + `tags/popular/20`)
        .then(response => {
            if(response.ok){
                return response;
            }
            else {
                var error = new Error('Error ' + response.status  + ' : ' + response.statusText)
                error.message = response;
                throw error;
            }
        }, err => {
            var error = new Error(err.message)
            throw error;
        })
        .then(response => response.json())
        .then(popularTags => dispatch(getPopularTags(popularTags)))
        .catch(error => dispatch(popularTagsFailed(error)))
}
export const  popularTagsLoading = () => ({
    type : ActionTypes.POPULAR_POST_LOADING
})
export const getPopularTags = (popularTags) => ({
    type : ActionTypes.GET_POPULAR_TAGS,
    payload: popularTags
})
export const popularTagsFailed = (err) => ({
    type : ActionTypes.POPULAR_TAGS_FAILED,
    payload: err
})
//Search Query Action
export const fetchSearchResults = (statusId, search) => (dispatch) => {
	dispatch(searchLoading());
	return fetch(baseUrl + `post?StatusId=${statusId}&Search=${search}`)
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(err) => {
				var err = new Error(err.message);
				throw err;
			}
		)
		.then((response) => response.json())
		.then((results) => dispatch(getSearchResults(results)))
		.catch((err) => dispatch(searchFailed(err.message)));
};

export const searchLoading = () => ({
	type: ActionTypes.SEARCH_LOADING
});
export const getSearchResults = (results) => ({
	type: ActionTypes.GET_SEARCHRESULTS,
	payload: results
});
export const searchFailed = (err) => ({
	type: ActionTypes.SEARCH_FAILED,
	payload: err
});
export const clearSearch = () => ({
	type: ActionTypes.SEARCH_CLEAR_RESULTS
});
//Increase View
export const increaseView = (id) => (dispatch) => {
	const postId = JSON.stringify(id);
	return fetch(baseUrl + `post/${postId}/views/increase`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		credentials: 'same-origin'
	})
		.then(
			(response) => {
				if (response.ok) {
                    console.log('Succcfully increased view');
                    return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(err) => {
				var errMess = new Error(error.message);
				throw errMess;
			}
		)
		.then((response) => response.json())
		.catch((error) => dispatch(increaseViewFailed(error.message)));
};

export const increaseViewFailed = (err) => ({
	type: ActionTypes.INCREASE_VIEW,
	payload: err
});
