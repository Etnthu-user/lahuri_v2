import * as ActionTypes from './ActionTypes'


export const sponsers = (state = 
    {
        isLoading : true,
        error : null,
        advert: []
    }, action) =>{
        switch(action.type){
            case ActionTypes.GET_SPONSERS:
                return{...state, isLoading:false, advert : action.payload}
            case ActionTypes.SPONSERS_LOADING:
                return {...state, isLoading:true}
            case ActionTypes.SPONSERS_FAILED:
                return {...state, isLoading:false, error:action.payload}
            default:
                return {...state}
        }
    }