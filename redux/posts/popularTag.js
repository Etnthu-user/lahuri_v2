import * as ActionTypes from '../ActionTypes'

export const popularTags = (state =
    {
        isLoading:true,
        error: null,
        tags: []
    }, action) => {
        switch(action.type){
            case ActionTypes.GET_POPULAR_TAGS:
                return {...state, isLoading:false, tags: action.payload}
            case ActionTypes.POPULAR_TAGS_LOADING:
                return {...state, isLoading:true}
            case ActionTypes.POPULAR_TAGS_FAILED:
                return {...state, isLoading:false , error: action.payload}
            default:
                return{...state}
        }
    }