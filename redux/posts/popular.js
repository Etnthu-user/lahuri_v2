import * as ActionTypes from '../ActionTypes'

export const popular = (state = 
    {
        isLoading : true,
        error : null,
        posts: []
    },action) => {
        switch(action.type){
            case ActionTypes.GET_POPULAR_POST:
                return {...state, isLoading: false, error: null, posts : action.payload}
            case ActionTypes.POPULAR_POST_LOADING:
                return {...state, isLoading: true}
            case ActionTypes.POPULAR_POST_FAILED:
                return {...state, isLoading:false, error:action.payload}
            default:
                return state
        }
    }