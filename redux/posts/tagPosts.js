import * as ActionTypes from '../ActionTypes'

export const tagPosts = (state =
    {
        posts: [],
        refreshing:true,
        isLastPage: false,
        nextPage: 2,
        isLoading: true,
        errMess : null
    }, action ) => {
        switch(action.type){
            case ActionTypes.GET_TAGPOSTS:
                return {...state, 
                    refreshing:false,
                    isLoading:false, 
                    isLastPage:action.payload.tagPosts.isLastPage,
                    nextPage:action.payload.tagPosts.nextPage,
                    posts : action.payload.append 
                    ?
                    state.posts.concat(action.payload.tagPosts.list)
                    :
                    action.payload.tagPosts.list, 
                    errMess:null}
            case ActionTypes.TAGPOSTS_LOADING:
                return {...state, isLoading:true,refreshing:false}
            case ActionTypes.TAGPOSTS_FAILED:
                return {...state, isLoading:false, errMess:action.payload}
            default:
                return {...state}
        }
    }