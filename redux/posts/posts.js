import * as ActionTypes from '../ActionTypes'


export const posts = (state=
    {   
        isLastPage:false,
        nextPage:2,
        refreshing:true,
        isLoading:true,
        error:null,
        posts:[]
    }
    ,action) =>{
    switch(action.type){
        case ActionTypes.GET_POSTS:
            return {...state,
                isLastPage:action.payload.posts.isLastPage,
                nextPage:action.payload.posts.nextPage,
                refreshing:false, isLoading:false, 
                posts:action.payload.append
                ?state.posts.concat(action.payload.posts.list)
                :action.payload.posts.list,
                error:null
            }
        case ActionTypes.POST_LOADING:
            return {...state, isLoading:true}
        case ActionTypes.INCREASE_VIEW:
			return { ...state };
        case ActionTypes.POSTS_FAILED:
            return{...state, isLoading:false ,error:action.payload}
        default:
            return state;
    }
}
