import * as ActionTypes from './ActionTypes';

export const search = (
	state = {
		isLoading: true,
		error: null,
		searchResults: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.GET_SEARCHRESULTS:
			return { ...state, isLoading: false, error: null, searchResults: action.payload };
		case ActionTypes.SEARCH_LOADING:
			return { ...state, error: null, isLoading: true };
		case ActionTypes.SEARCH_FAILED:
			return { ...state, isLoading: false, error: action.payload };
		case ActionTypes.SEARCH_CLEAR_RESULTS:
			return { ...state, searchResults: [] };
		default:
			return state;
	}
};
