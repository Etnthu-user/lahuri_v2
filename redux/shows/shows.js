import * as ActionTypes from '../ActionTypes';

export const shows = (state={isLoading:true,error:null, shows:[]},action) => {
    switch(action.type){
        case ActionTypes.GET_SHOWS:
            return {...state,isLoading:false,shows:action.payload}
        case ActionTypes.SHOWS_LOADING:
            return {...state, isLoading:true}
        case ActionTypes.SHOWS_FAILED:
            return {...state,isLoading:false, error:action.payload}
        default:
            return state;
    }
}