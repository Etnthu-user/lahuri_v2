import * as ActionTypes from '../ActionTypes';

export const episodes = (state ={
    nextPage: 'CBQQAA',
    isLoading:true,
    error : null, 
    episodes:[],
    refreshing:true,
    isLastPage:false
},action)=>{
    switch(action.type){
        case ActionTypes.GET_EPISODES:
            return {...state,isLoading:false,refreshing:false,isLastPage:action.payload.episodes.isLastPage,nextPage:action.payload.episodes.nextPage,
                 episodes:action.payload.append ? state.episodes.concat(action.payload.episodes.list):action.payload.episodes.list}
        case ActionTypes.EPISODES_LOADING:
            return{...state, isLoading:true }
        case ActionTypes.EPISODES_FAILED:
            return{...state,isLoading:false,error: action.payload}
        default:
            return state
    }
}