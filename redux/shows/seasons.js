import * as ActionTypes from '../ActionTypes'

export const seasons = (state={isLoading:true,error: null,seasons:[]},action) => {
    switch(action.type){
        case ActionTypes.GET_SEASONS:
            return {...state,isLoading:false,seasons:action.payload}
        case ActionTypes.SEASONS_LOADING:
            return {...state,isLoading:true,}
        case ActionTypes.SEASONS_FAILED:
            return {...state, isLoading:false,error:action.payload}
        default:
            return state
    }
}