import {createStore,combineReducers,applyMiddleware} from 'redux'
import {persistStore, persistCombineReducers} from 'redux-persist'
import storage from 'redux-persist/es/storage';
import thunk from 'redux-thunk';
import { posts } from './posts/posts';
import { shows } from './shows/shows';
import { seasons } from './shows/seasons';
import { episodes } from './shows/episodes';
import { popular } from './posts/popular';
import { tagPosts } from './posts/tagPosts';
import { sponsers } from './sponsers';
import { popularTags } from './posts/popularTag';
import { search } from './search';


export const configureStore = () => {
    const config = {
        key: 'root',
        storage,
    };
    const store = createStore(
        persistCombineReducers(config,{
            posts,
            shows,
            seasons,
            episodes,
            popular,
            tagPosts,
            sponsers,
            popularTags,
            search
        }),applyMiddleware(thunk)
    );
    const persistor = persistStore(store);
    return {store, persistor};
}