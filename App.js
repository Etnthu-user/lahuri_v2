import React from 'react';
import {Provider} from 'react-redux';
import {configureStore} from './redux/configureStore';
import {PersistGate} from 'redux-persist/es/integration/react'
import Main from './components/Main';
import { Loading } from './components/sharedComponents/Loading';
const {store, persistor} = configureStore();
export default class App extends React.Component {
  render() {
    return (
      <Provider store = {store}>
        <PersistGate loading ={<Loading/>} persistor ={persistor}>
          <Main/>
        </PersistGate>
      </Provider>
    );
  }
}